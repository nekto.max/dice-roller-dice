import React from 'react'
import PropTypes from 'prop-types'

function ControlPanel({
  diceList,
  customDice,
  rerollPermission,
  diceCount,
  diceMod,
  diceType,
  diceMalus,
  handleDiceCount,
  handleDiceMod,
  handleCustomDice,
  handleRerollPermission,
  handleDiceType,
  handleResetParams,
  handleCustomDiceType,
  handleDiceModType,
  handleRollDice,
  handleClearLog,
}) {
  return (
    <form className="control-panel">
      <div className="control-panel__options">
        <label htmlFor="dice-edge">Ввод грани:</label>
        <input
          id="dice-edge"
          name="dice-edge"
          type="checkbox"
          checked={customDice}
          onChange={(e) => handleCustomDice(e.target.checked)}
        />
        <label htmlFor="dice-reroll">Переброс:</label>
        <input
          id="dice-reroll"
          name="dice-reroll"
          type="checkbox"
          checked={rerollPermission}
          onChange={(e) => handleRerollPermission(e.target.checked)}
        />
      </div>
      <div className="control-panel__params">
        <div>
          <label htmlFor="dice-num">Количество:</label>
          <input
            id="dice-num"
            name="dice-num"
            type="number"
            min="1"
            value={diceCount}
            onChange={(e) => handleDiceCount(e.target.value)}
            disabled={rerollPermission}
          />
        </div>
        <div>
          <label htmlFor="dice-type">Кость:</label>
          {
            customDice ? (
              <input
                id="dice-type"
                name="dice-type"
                type="number"
                min="2"
                value={diceType}
                onChange={(e) => handleCustomDiceType(e.target.value)}
                disabled={rerollPermission}
              />
            ) : (
              <select
                id="dice-type"
                name="dice-type"
                size="1"
                value={diceType}
                onChange={(e) => handleDiceType(e.target.value)}
                disabled={rerollPermission}
              >
                {
                  diceList.map((item) => <option key={item} value={item}>{`d${item}`}</option>)
                }
              </select>
            )
          }
        </div>
        <div>
          {/* <img src={diceMalus ? icon.minus : icon.plus} className="modtype" alt="modtype" onClick={handleDiceModType} /> */}
          <span role="img" className="modtype" onClick={handleDiceModType}>{diceMalus ? '-' : '+'}</span>
          <label htmlFor="dice-mod">Модификатор:</label>
          <input
            id="dice-mod"
            name="dice-mod"
            type="number"
            min="0"
            value={diceMod}
            onChange={(e) => handleDiceMod(e.target.value)}
            disabled={rerollPermission}
          />
        </div>
      </div>
      <div className="control-panel__actions">
        <button type="button" onClick={handleRollDice}>Бросить</button>
        <div>
          <button type="button" onClick={handleResetParams}>Сбросить<br />параметры</button>
          <button type="button" onClick={handleClearLog}>Очистить<br />лог</button>
        </div>
      </div>
    </form>
  )
}

ControlPanel.defaultProps = {
  handleDiceCount: () => {},
  handleDiceMod: () => {},
  handleCustomDice: () => {},
  handleRerollPermission: () => {},
  handleDiceType: () => {},
  handleResetParams: () => {},
  handleCustomDiceType: () => {},
  handleDiceModType: () => {},
  handleRollDice: () => {},
  handleClearLog: () => {},
  diceList: [],
  diceCount: '0',
  diceMod: '0',
  diceType: 'd6',
  customDice: false,
  rerollPermission: false,
  diceMalus: false,
}

ControlPanel.propTypes = {
  handleDiceCount: PropTypes.func,
  handleDiceMod: PropTypes.func,
  handleCustomDice: PropTypes.func,
  handleRerollPermission: PropTypes.func,
  handleDiceType: PropTypes.func,
  handleResetParams: PropTypes.func,
  handleCustomDiceType: PropTypes.func,
  handleDiceModType: PropTypes.func,
  handleRollDice: PropTypes.func,
  handleClearLog: PropTypes.func,
  diceList: PropTypes.array,
  diceCount: PropTypes.string,
  diceMod: PropTypes.string,
  diceType: PropTypes.string,
  customDice: PropTypes.bool,
  rerollPermission: PropTypes.bool,
  diceMalus: PropTypes.bool,
}

export default ControlPanel
