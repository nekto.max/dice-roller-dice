import React from 'react'
import PropTypes from 'prop-types'

function DicePanel({
  diceResults,
  handleRerollDice,
  rerollPermission,
}) {
  return (
    <div className="dice-panel">
      <div className="dice-throw">
        {
          (diceResults.length > 0) && (
            diceResults.map((item, number) => (
              <div
                className={`dice-throw__dice${rerollPermission ? ' roll' : ''}`}
                role="button"
                tabIndex={0}
                id={`${item}${String(Math.random()).slice(2)}`}
                key={`${item}${String(Math.random()).slice(2)}`}
                onClick={() => handleRerollDice(number)}
              >
                <span>{item}</span>
              </div>
            ))
          )
        }
      </div>
    </div>
  )
}

DicePanel.defaultProps = {
  handleRerollDice: () => {},
  diceResults: [],
  rerollPermission: false,
}

DicePanel.propTypes = {
  handleRerollDice: PropTypes.func,
  diceResults: PropTypes.array,
  rerollPermission: PropTypes.bool,
}

export default DicePanel
