import React from 'react'
import PropTypes from 'prop-types'

function LogPanel({
  sumThrow,
  logResults,
}) {
  return (
    <div className="log-panel">
      <span className="log-panel__label-log">Результат броска:</span>
      <span className="log-panel__last-log">{sumThrow}</span>
      <textarea className="log-panel__all-log" value={logResults} readOnly />
    </div>
  )
}

LogPanel.defaultProps = {
  sumThrow: '',
  logResults: '',
}

LogPanel.propTypes = {
  sumThrow: PropTypes.string,
  logResults: PropTypes.string,
}

export default LogPanel
