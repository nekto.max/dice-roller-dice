import React from 'react'
// import PropTypes from 'prop-types';

import ControlPanel from '../../containers/ControlPanel'
import DicePanel from '../../containers/DicePanel'
import LogPanel from '../../containers/LogPanel'

function Main() {
    return (
      <div className="main-block">
        <ControlPanel />
        <DicePanel />
        <LogPanel />
      </div>
    );
}

// Main.defaultProps = {
//   onClick: () => {},
//   isHideHeader: false,
// }

// Main.propTypes = {
//   onClick: PropTypes.func,
//   isHideHeader: PropTypes.bool,
// }

export default Main
