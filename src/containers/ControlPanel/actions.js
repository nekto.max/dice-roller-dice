import * as actionTypes from './constants'

/** Action creators */

export const resetDiceParams = () => ({ type: actionTypes.RESET_DICE_PARAMS })

export const setCustomDice = (data) => ({ type: actionTypes.SET_CUSTOM_DICE, payload: data })
export const setRerollPermission = (data) => ({ type: actionTypes.SET_REROLL_PERMISSION, payload: data })

export const setDiceCount = (data) => ({ type: actionTypes.SET_DICE_COUNT, payload: data })
export const setDiceType = (data) => ({ type: actionTypes.SET_DICE_TYPE, payload: data })
export const setDiceMod = (data) => ({ type: actionTypes.SET_DICE_MOD, payload: data })
export const setDiceModType = (data) => ({ type: actionTypes.SET_DICE_MOD_TYPE, payload: data })
