/** Types of actions */

const moduleName = 'control'

export const RESET_DICE_PARAMS = `${moduleName}/reset_dice_params:REQUEST`

export const SET_CUSTOM_DICE = `${moduleName}/set_custom_dice_flag:REQUEST`
export const SET_REROLL_PERMISSION = `${moduleName}/set_reroll_permission:REQUEST`

export const SET_DICE_COUNT = `${moduleName}/set_dice_count:REQUEST`
export const SET_DICE_TYPE = `${moduleName}/set_dice_type:REQUEST`
export const SET_DICE_MOD = `${moduleName}/set_dice_mod:REQUEST`
export const SET_DICE_MOD_TYPE = `${moduleName}/set_dice_mod_type:REQUEST`
