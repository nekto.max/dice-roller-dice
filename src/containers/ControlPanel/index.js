import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

import {
  diceList,
} from '../../constants'

import {
  randomDice,
} from '../../utils'

import {
  customDice,
  rerollPermission,
  diceCount,
  diceMod,
  diceType,
  diceMalus,
} from '../../store/selectors'

import {
  setCustomDice,
  setRerollPermission,
  setDiceCount,
  setDiceMod,
  setDiceType,
  resetDiceParams,
  setDiceModType,
  setDiceResults,
  clearLogResults,
} from '../../store/actions'

import ControlPanelComponent from '../../components/ControlPanel'

function ControlPanel() {
  const dispatch = useDispatch()
  const customDiceSelector = useSelector(customDice)
  const rerollPermissionSelector = useSelector(rerollPermission)
  const diceCountSelector = useSelector(diceCount)
  const diceModSelector = useSelector(diceMod)
  const diceTypeSelector = useSelector(diceType)
  const diceMalusSelector = useSelector(diceMalus)

  const setDiceParam = (value, callback, min = '0') => {
    const valueNoZero = value.replace(/^0+/, '')
    if (value === '') dispatch(callback(min))
    else if (/^[0-9]+$/.test(valueNoZero)) dispatch(callback(valueNoZero))
  }

  const handleDiceCount = (value) => setDiceParam(value, setDiceCount, '1')

  const handleDiceMod = (value) => setDiceParam(value, setDiceMod)

  const handleCustomDice = (checked) => {
    dispatch(setCustomDice(checked))
    if (!checked && !diceList.includes(diceTypeSelector)) dispatch(setDiceType('6'))
  }

  const handleRerollPermission = (checked) => dispatch(setRerollPermission(checked))

  const handleDiceType = (value) => dispatch(setDiceType(value))

  const handleCustomDiceType = (value) => setDiceParam(value, setDiceType, '2')

  const handleResetParams = () => dispatch(resetDiceParams())

  const handleDiceModType = () => dispatch(setDiceModType(!diceMalusSelector))

  const handleRollDice = () => {
    const res = Array.from({ length: +diceCountSelector }, () => randomDice(diceTypeSelector, diceModSelector, diceMalusSelector))
    dispatch(setDiceResults(res))
  }

  const handleClearLog = () => dispatch(clearLogResults())

  return (
    <ControlPanelComponent
      diceList={diceList}
      customDice={customDiceSelector}
      rerollPermission={rerollPermissionSelector}
      diceCount={diceCountSelector}
      diceMod={diceModSelector}
      diceType={diceTypeSelector}
      diceMalus={diceMalusSelector}
      handleDiceCount={handleDiceCount}
      handleDiceMod={handleDiceMod}
      handleCustomDice={handleCustomDice}
      handleRerollPermission={handleRerollPermission}
      handleDiceType={handleDiceType}
      handleResetParams={handleResetParams}
      handleCustomDiceType={handleCustomDiceType}
      handleDiceModType={handleDiceModType}
      handleRollDice={handleRollDice}
      handleClearLog={handleClearLog}
    />
  )
}

export default ControlPanel
