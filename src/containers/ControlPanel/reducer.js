import * as actionTypes from './constants';

/** Initial state */

export const INITIAL_STATE = {
  // опции броска
  customDice: false,
  rerollPermission: false,
  // формула броска
  diceCount: '1',
  diceType: '6',
  diceMod: '0',
  diceMalus: false,
}

/** Reducer */

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    //
    case actionTypes.SET_CUSTOM_DICE:
      return {
        ...state,
        customDice: action.payload,
      }
    case actionTypes.SET_REROLL_PERMISSION:
      return {
        ...state,
        rerollPermission: action.payload,
      }
    //
    case actionTypes.SET_DICE_COUNT:
      return {
        ...state,
        diceCount: action.payload,
      }
    case actionTypes.SET_DICE_TYPE:
      return {
        ...state,
        diceType: action.payload,
      }
    case actionTypes.SET_DICE_MOD:
      return {
        ...state,
        diceMod: action.payload,
      }
    case actionTypes.SET_DICE_MOD_TYPE:
      return {
        ...state,
        diceMalus: action.payload,
      }
    case actionTypes.RESET_DICE_PARAMS:
      return {
        ...state,
        customDice: false,
        rerollPermission: false,
        diceCount: '1',
        diceType: '6',
        diceMod: '0',
        diceMalus: false,
      }
    //
    default:
      return state
  }
}
