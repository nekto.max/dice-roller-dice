import { createSelector } from 'reselect';

/** Selectors */

const stateSelector = (state) => state.control

export const customDice = createSelector(stateSelector, state => state.customDice)
export const rerollPermission = createSelector(stateSelector, state => state.rerollPermission)

export const diceCount = createSelector(stateSelector, state => state.diceCount)
export const diceType = createSelector(stateSelector, state => state.diceType)
export const diceMod = createSelector(stateSelector, state => state.diceMod)
export const diceMalus = createSelector(stateSelector, state => state.diceMalus)
