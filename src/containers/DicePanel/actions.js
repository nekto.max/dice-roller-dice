import * as actionTypes from './constants'

/** Action creators */

export const setDiceResults = (data) => ({ type: actionTypes.SET_DICE_RESULTS, payload: data })
export const setLogResults = (data) => ({ type: actionTypes.SET_LOG_RESULTS, payload: data })
export const clearLogResults = () => ({ type: actionTypes.CLEAR_LOG_RESULTS })
