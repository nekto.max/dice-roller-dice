/** Types of actions */

const moduleName = 'dice'

export const SET_DICE_RESULTS = `${moduleName}/set_dice_results:REQUEST`
export const SET_LOG_RESULTS = `${moduleName}/set_log_results:REQUEST`
export const CLEAR_LOG_RESULTS = `${moduleName}/clear_log_results:REQUEST`
