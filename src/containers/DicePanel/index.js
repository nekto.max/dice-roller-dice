import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

import {
  randomDice,
} from '../../utils'

import {
  diceResults,
  diceMalus,
  diceType,
  diceMod,
  rerollPermission,
} from '../../store/selectors'

import {
  setDiceResults,
} from '../../store/actions'

import DicePanelComponent from '../../components/DicePanel'

function DicePanel() {
  const dispatch = useDispatch()
  const diceResultsSelector = useSelector(diceResults)
  const diceModSelector = useSelector(diceMod)
  const diceTypeSelector = useSelector(diceType)
  const diceMalusSelector = useSelector(diceMalus)
  const rerollPermissionSelector = useSelector(rerollPermission)

  const handleRerollDice = (number) => {
    if (rerollPermissionSelector) {
      const newResults = diceResultsSelector.slice()
      newResults.splice(number, 1, randomDice(diceTypeSelector, diceModSelector, diceMalusSelector))
      dispatch(setDiceResults(newResults))
    }
  }

  return (
    <DicePanelComponent
      diceResults={diceResultsSelector}
      handleRerollDice={handleRerollDice}
      rerollPermission={rerollPermissionSelector}
    />
  )
}

export default DicePanel
