import * as actionTypes from './constants';

/** Initial state */

export const INITIAL_STATE = {
  diceResults: [],
  logResults: '',
}

/** Reducer */

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    //
    case actionTypes.SET_DICE_RESULTS:
      return {
        ...state,
        diceResults: action.payload,
      }
    case actionTypes.SET_LOG_RESULTS:
      return {
        ...state,
        logResults: action.payload,
      }
    case actionTypes.CLEAR_LOG_RESULTS:
      return {
        ...state,
        logResults: '',
      }
    //
    default:
      return state
  }
}
