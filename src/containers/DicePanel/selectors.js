import { createSelector } from 'reselect';

/** Selectors */

const stateSelector = (state) => state.dice

export const diceResults = createSelector(stateSelector, state => state.diceResults)
export const logResults = createSelector(stateSelector, state => state.logResults)
