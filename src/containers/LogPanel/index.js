import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import moment from 'moment'

import {
  diceResults,
  logResults,
} from '../../store/selectors'

import {
  setLogResults,
} from '../../store/actions'

import LogPanelComponent from '../../components/LogPanel'

function LogPanel() {
  const dispatch = useDispatch()
  const diceResultsSelector = useSelector(diceResults)
  const logResultsSelector = useSelector(logResults)

  const [sumThrow, setSumThrow] = useState('')

  useEffect(() => {
    let currentThrow = ''
    let resultThrow = ''
    if (diceResultsSelector.length === 1) {
      resultThrow = diceResultsSelector[0]
      currentThrow = resultThrow
    } else if (diceResultsSelector.length > 1) {
      resultThrow = `${diceResultsSelector.reduce((res, item) => res + +item, 0)}`
      currentThrow = `${diceResultsSelector.join(' + ')} = ${resultThrow}`
    }
    setSumThrow(resultThrow)
    if (currentThrow !== '') dispatch(setLogResults(`${moment().format('DD.MM.YYYY hh:mm:ss')}: ${currentThrow}\n${logResultsSelector}`))
  }, [diceResultsSelector])

  return (
    <LogPanelComponent
      sumThrow={sumThrow}
      logResults={logResultsSelector}
    />
  )
}

export default LogPanel
