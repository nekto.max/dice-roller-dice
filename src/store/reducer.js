import { combineReducers } from 'redux'

import control from '../containers/ControlPanel/reducer'
import dice from '../containers/DicePanel/reducer'

export default combineReducers({
  control,
  dice,
})
