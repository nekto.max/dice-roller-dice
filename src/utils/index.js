// const randomPureDice = (edge) => Math.floor(1 + Math.random() * (edge))

export const randomDice = (edge, mod, malus = false) => {
  const dice = Math.floor(1 + Math.random() * (+edge))
  return String(malus ? dice - +mod : dice + +mod)
}
